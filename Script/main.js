
function openNav() {
    document.getElementById("mySidenav").style.width = "20em";
    document.querySelector(".opacity").style.width = "100vw";
    document.body.style.backgroundColor = "rgba(0,0,0,0.2)";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.querySelector(".opacity").style.width = "0";
    document.body.style.backgroundColor = "white";
}

document.querySelector(".opacity").addEventListener('click', closeNav);
document.querySelector("#closebtn").addEventListener('click', closeNav);
document.querySelector(".menu-btn").addEventListener('click', openNav);