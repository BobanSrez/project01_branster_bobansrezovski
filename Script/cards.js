
let games = [
    {
        link: "Html/game1.html",
        slika: "Img/01.png",
        naslov: "Dotmocracy",
        kategorija: "Акција",
        vreme: "5-30 минути"
    },
    {
        link: "Html/game2.html",
        slika: "Img/02.png",
        naslov: "Project Mid-way Evaluation",
        kategorija: "Акција",
        vreme: "30-60 минути"
    },
    {
        link: "Html/game3.html",
        slika: "Img/03.png",
        naslov: "Тhe 5 Whys",
        kategorija: "Иновација",
        vreme: "30-60 минути"
    },
    {
        link: "Html/game4.html",
        slika: "Img/04.png",
        naslov: "Future Trends",
        kategorija: "Иновација",
        vreme: "60-120 минути"
    },
    {
        link: "Html/game5.html",
        slika: "Img/05.png",
        naslov: "Story Building",
        kategorija: "Иновација",
        vreme: "30-60 минути"
    },
    {
        link: "Html/game6.html",
        slika: "Img/06.png",
        naslov: "Тake a Stand",
        kategorija: "Иновација",
        vreme: "60-120 минути"
    },
    {
        link: "Html/game7.html",
        slika: "Img/07.png",
        naslov: "IDOARRT Meeting Design",
        kategorija: "Акција",
        vreme: "5-30 минути"
    },
    {
        link: "Html/game8.html",
        slika: "Img/08.png",
        naslov: "I3 Action Steps",
        kategorija: "Акција",
        vreme: "120-240 минути"
    },
    {
        link: "Html/game9.html",
        slika: "Img/09.png",
        naslov: "Letter to Myself",
        kategorija: "Иновација",
        vreme: "5-30 минути"
    },
    {
        link: "Html/game10.html",
        slika: "Img/10.png",
        naslov: "Аctive Listening",
        kategorija: "Лидерство",
        vreme: "60-120 минути"
    },
    {
        link: "Html/game11.html",
        slika: "Img/11.png",
        naslov: "Feedback: I appreciate",
        kategorija: "Лидерство",
        vreme: "60-120 минути"
    },
    {
        link: "Html/game12.html",
        slika: "Img/12.png",
        naslov: "Explore your values",
        kategorija: "Лидерство",
        vreme: "60-120 минути"
    },
    {
        link: "Html/game13.html",
        slika: "Img/13.png",
        naslov: "Reflection Individual",
        kategorija: "Лидерство",
        vreme: "30-60 минути"
    },
    {
        link: "Html/game14.html",
        slika: "Img/14.png",
        naslov: "Back-turned Feedback",
        kategorija: "Лидерство",
        vreme: "60-120 минути"
    },
    {
        link: "Html/game15.html",
        slika: "Img/15.png",
        naslov: "Conflict Responses",
        kategorija: "Тим",
        vreme: "60-120 минути"
    },
    {
        link: "Html/game16.html",
        slika: "Img/16.png",
        naslov: "Myers-Briggs Team Reflection",
        kategorija: "Тим",
        vreme: "60-120 минути"
    },
    {
        link: "Html/game17.html",
        slika: "Img/17.png",
        naslov: "Personal Presentations",
        kategorija: "Тим",
        vreme: "60-240 минути"
    },
    {
        link: "Html/game18.html",
        slika: "Img/18.png",
        naslov: "Circles of influence",
        kategorija: "Тим",
        vreme: "30-120 минути"
    }
];

let contentElement = document.getElementById('section');
let contents = "";
games.forEach(element => {
    contents += `
        <div class="col-4k-3 col-lg-4 col-md-6 col-sm-6">
            <a href="`+ element.link + `">
                <div class="card">
                    <div class="card-header">
                        <img class="card-img-top" src=`+ element.slika + ` alt="Card image cap">
                    </div>
                    <div class="card-body">
                        <div class="media media-lp">
                            <div class="media-body">
                                <h5 class="mt-0 mb-1">`+ element.naslov + `</h5>
                                <p>Категорија: <span>`+ element.kategorija + `</span></p>
                            </div>
                            <div class="media-right">
                                <img class="card-img-top" src=`+ element.slika + ` alt="Card image cap">
                            </div>
                        </div>
                        <div class="media media-lp">
                            <div class="media-left">
                                <i class="far fa-clock fa-2x"></i>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Времтраење</h4>
                                <p class="time">`+ element.vreme + `</p>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
`;
});

contentElement.innerHTML = contents;



